"use client";

import { FcGoogle } from "react-icons/fc";
import { signIn } from "next-auth/react";
import Button from "../shared/Button";

const Login = () => {
  return (
    <div>
      <Button
        label="Sign in with"
        icon={FcGoogle}
        handleClick={() => signIn("google")}
      />
    </div>
  );
};

export default Login;
