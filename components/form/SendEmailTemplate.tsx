"use client";

import { toast } from "react-toastify";
import { RiMailSendLine } from "react-icons/ri";
import { useEffect, useState } from "react";
import { sendEmailTemplate } from "@/lib/actions/send.action";
import MultiEmail from "../shared/MultiEmail";
import FileUpload, { UploadableFile } from "../shared/FileUpload";
import Button from "../shared/Button";
import "react-multi-email/dist/style.css";

const SendEmailTemplate = () => {
  const [emailTemplate, setEmailTemplate] = useState<UploadableFile[]>([]);
  const [emails, setEmails] = useState<string[]>([]);

  const onFormSubmit = async () => {
    if (!emails.length) {
      toast.error("Email is missing.");
      return;
    }

    if (!emailTemplate.length || emailTemplate.length !== 1) {
      toast.error("Required 1 Email Template html file.");
      return;
    }

    if (emailTemplate[0]?.errors?.length || !emailTemplate[0].url) {
      toast.error("Error in uploaded file.");
      return;
    }

    const { url: fileUrl } = emailTemplate[0];

    try {
      const response = await sendEmailTemplate({
        emails,
        fileUrl,
      });
      setEmails(() => []);
      setEmailTemplate(() => []);
      toast.success("Email sent successfully, check your email.");
    } catch (error: any) {
      toast.error(error.message);
    }
  };

  return (
    <div className="min-w-[300px] max-w-[600px] w-full flex flex-col gap-16">
      <div className="flex flex-col gap-8">
        <MultiEmail handleMultiEmailChange={setEmails} />

        <FileUpload
          handleUploadedFiles={setEmailTemplate}
          allowedFileType={{ "text/html": [".html"] }}
          maxFiles={1}
          classNames="group transition border hover:bg-slate-300 hover:border-black rounded-md cursor-pointer"
        />
      </div>

      <Button icon={RiMailSendLine} label="Send" handleClick={onFormSubmit} />
    </div>
  );
};

export default SendEmailTemplate;
