"use client";

import { LuLogOut } from "react-icons/lu";
import { signOut } from "next-auth/react";
import Button from "../shared/Button";

const AdminMenu = async () => {
  return (
    <div>
      <Button label="logout" icon={LuLogOut} handleClick={() => signOut()} />
    </div>
  );
};

export default AdminMenu;
