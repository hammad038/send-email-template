import Logo from "./Logo";
import AdminMenu from "./AdminMenu";
import { getSession } from "@/lib/actions/user.actions";

const Header = async () => {
  const session = await getSession();

  return (
    <header>
      <nav>
        <div className="flex flex-row justify-between items-center py-4">
          <Logo />

          {session?.user ? <AdminMenu /> : null}
        </div>
      </nav>
    </header>
  );
};

export default Header;
