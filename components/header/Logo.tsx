import Image from "next/image";
import Link from "next/link";

const Logo = () => (
  <Link href="/">
    <Image
      src="/assets/sendemailtemplate-logo.svg"
      width={64}
      height={64}
      alt="site logo"
    />
  </Link>
);

export default Logo;
