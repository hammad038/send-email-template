import { ReactMultiEmail } from "react-multi-email";
import { useEffect, useState } from "react";

interface MultiEmailProps {
  handleMultiEmailChange: (emailsList: string[]) => void;
}

const MultiEmail: React.FC<MultiEmailProps> = ({ handleMultiEmailChange }) => {
  const [emails, setEmails] = useState<string[]>([]);
  const [focused, setFocused] = useState(false);

  useEffect(() => {
    handleMultiEmailChange(emails);
  }, [emails]);

  return (
    <ReactMultiEmail
      className="focus-within:!border-black"
      placeholder="abc@gamil.com, xyz@gmx.de, ..."
      emails={emails}
      onChange={(_emails: string[]) => {
        setEmails(_emails);
      }}
      autoFocus={true}
      onFocus={() => setFocused(true)}
      onBlur={() => setFocused(false)}
      getLabel={(email, index, removeEmail) => {
        return (
          <div
            data-tag
            key={index}
            className="!bg-black !text-white !font-light !leading-4"
          >
            <div data-tag-item>{email}</div>
            <span data-tag-handle onClick={() => removeEmail(index)}>
              ×
            </span>
          </div>
        );
      }}
    />
  );
};

export default MultiEmail;
