"use client";

import { IconType } from "react-icons";

interface ButtonProps {
  label?: string;
  icon?: IconType;
  iconProps?: { size?: number; color?: string };
  handleClick: () => void;
  type?: "submit" | "reset" | "button";
  disabled?: boolean;
  classNames?: string;
}

const DEFAULT_ICON = {
  size: 16,
  color: "white",
};

const Button: React.FC<ButtonProps> = ({
  label,
  icon: Icon,
  iconProps,
  handleClick,
  type = "button",
  disabled,
  classNames,
}) => {
  if (!Icon && !label) return null;

  return (
    <button
      type={type}
      onClick={handleClick}
      disabled={disabled}
      className={`w-full bg-black text-white hover:bg-slate-700 rounded-xl
        ${disabled && "disabled:bg-gray-300 disabled:cursor-not-allowed"}
        ${classNames ?? ""}
      `}
    >
      <div className="flex flex-row items-center justify-center gap-2 px-8 py-2">
        {label}

        {Icon && (
          <Icon
            size={iconProps?.size || DEFAULT_ICON.size}
            color={iconProps?.color || DEFAULT_ICON.color}
          />
        )}
      </div>
    </button>
  );
};

export default Button;
