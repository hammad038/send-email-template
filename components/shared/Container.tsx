interface ContainerProps {
  children: React.ReactNode;
  classNames?: string;
}
const Container = ({ children, classNames }: ContainerProps) => {
  return (
    <div
      className={`max-w-[2520px] mx-auto w-full xl:px:28 md:px-10 sm:px-6 px-4 ${
        classNames ?? ""
      }`}
    >
      {children}
    </div>
  );
};

export default Container;
