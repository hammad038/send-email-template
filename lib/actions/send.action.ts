"use server";

import { sendMail } from "../utils/nodemailer";

interface Props {
  emails: string[];
  fileUrl: string;
}

export const sendEmailTemplate = async ({ emails, fileUrl }: Props) => {
  try {
    if (emails.length <= 0) {
      throw new Error("No email is provided.");
    }

    if (!fileUrl) {
      throw new Error("Error with uploaded html teamplate file.");
    }

    const htmlTemplate = await fetchHtmlFileContent(fileUrl);

    const sendingEmailResponse = await sendMail({
      to: emails,
      html: htmlTemplate,
    });

    return { emails, htmlTemplate, sendingEmailResponse };
  } catch (error: any) {
    throw new Error(error.message || "Server error");
  }
};

const fetchHtmlFileContent = async (htmlFileUrl: string): Promise<string> => {
  try {
    const response = await fetch(htmlFileUrl);
    return await response.text();
  } catch (error) {
    throw new Error("Error while fetching html file content from server.");
  }
};
