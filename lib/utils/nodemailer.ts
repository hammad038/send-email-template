const nodemailer = require("nodemailer");

interface Props {
  to: string[];
  html: string;
  from?: string;
  subject?: string;
}

export const sendMail = async ({ to, html, from, subject }: Props) => {
  // Create a nodemailer transporter
  const transporter = nodemailer.createTransport({
    host: process.env.GOOGLE_SMTP_HOST,
    port: process.env.GOOGLE_SMTP_PORT,
    auth: {
      user: process.env.GOOGLE_SMTP_USER,
      pass: process.env.GOOGLE_SMTP_PASS,
    },
    secure: true,
  });

  await new Promise((resolve, reject) => {
    // verify connection configuration
    transporter.verify(function (error: any, success: any) {
      if (error) {
        console.log(error);
        reject(error);
      } else {
        console.log("Server is ready to take our messages");
        resolve(success);
      }
    });
  });

  const mailOptions = {
    to,
    html: html || "<p>Something is wrong with template file.</p>",
    from: process.env.GOOGLE_SMTP_USER,
    subject: subject || "Email Template Test",
  };

  console.log({ to: mailOptions.to, from: mailOptions.from });

  // Sending email
  await new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (error: any, info: any) => {
      if (error) {
        console.log(error);
        reject(error);
      } else {
        console.log(info);
        resolve(info);
      }
    });
  });

  return true;
};
