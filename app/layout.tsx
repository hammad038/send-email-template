import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import ToastProvider from "@/components/shared/ToastProvider";
import Header from "@/components/header/Header";
import Container from "@/components/shared/Container";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Send Email Template",
  description: "Send email template",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body
        className={`${inter.className} min-h-screen flex flex-col`}
        suppressHydrationWarning={true}
      >
        <ToastProvider>
          <Container>
            <Header />
            {children}
          </Container>
        </ToastProvider>
      </body>
    </html>
  );
}
