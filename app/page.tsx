import { redirect } from "next/navigation";
import { getSession } from "@/lib/actions/user.actions";
import Login from "@/components/form/Login";

const Home = async () => {
  const session = await getSession();

  if (session) {
    redirect("/dashboard");
  }

  return (
    <main className="flex flex-col items-center justify-between p-24">
      <div className="flex items-center">
        <Login />
      </div>
    </main>
  );
};
export default Home;
