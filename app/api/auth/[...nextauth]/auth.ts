import { Account, NextAuthOptions, Profile, Session, User } from "next-auth";
import { PrismaAdapter } from "@next-auth/prisma-adapter";
import prisma from "@/lib/utils/prismadb";
import GoogleProvider from "next-auth/providers/google";
import { JWT } from "next-auth/jwt";
import { AdapterUser } from "next-auth/adapters";

const JWT_MAX_AGE = Number(process.env.JWT_VALIDITY_SECONDS) || 3600;

const adapter = PrismaAdapter(prisma);

const providers = [
  GoogleProvider({
    clientId: process.env.GOOGLE_CLIENT_ID as string,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET as string,
  }),
];

export const authOptions: NextAuthOptions = {
  secret: process.env.NEXTAUTH_SECRET,

  adapter,

  providers,

  pages: {
    signIn: "/",
    signOut: "/",
    error: "/error",
    newUser: "/dashboard",
  },

  session: {
    strategy: "jwt",
    maxAge: JWT_MAX_AGE,
  },

  callbacks: {
    async signIn({ user, account, profile, email, credentials }) {
      return true;
    },

    async redirect({ url, baseUrl }) {
      return baseUrl;
    },

    async session({
      session,
      user,
      token,
    }: {
      session: Session;
      user: User;
      token: JWT;
    }) {
      return session;
    },

    async jwt({
      token,
      user,
      account,
      profile,
      isNewUser,
    }: {
      token: JWT;
      user: User | AdapterUser;
      account: Account | null;
      profile?: Profile;
      isNewUser?: boolean | undefined;
    }): Promise<JWT> {
      return token;
    },
  },
};
