import SendEmailTemplate from "@/components/form/SendEmailTemplate";

const Dashboard = () => {
  return (
    <main className="flex flex-col items-center justify-between p-24">
      <SendEmailTemplate />
    </main>
  );
};
export default Dashboard;
